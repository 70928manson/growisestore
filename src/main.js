import { createApp } from 'vue'

import 'bootstrap'
import 'bootstrap-icons/font/bootstrap-icons.css'

import 'font-awesome/css/font-awesome.min.css'

import App from './App.vue'
import router from './router'

// import Vue from 'vue';
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

// // 安裝 BootstrapVue
// Vue.use(BootstrapVue);
// // 安裝 BootstrapVue icon components plugin (可選)
// Vue.use(IconsPlugin);

createApp(App).use(router).mount('#app')
